# QuoteFetch V1.0 #
This Library aims to provide real-time quotes from various sources, I've added support to get quotes from ICICIdirect and more to follow. This currently supports only Python 2.7 (Python 3 support planned)

## How to install ##

```
#!shell
$git clone https://swapnil_jariwala@bitbucket.org/swapnil_jariwala/quotefetch.git
$cd QuoteFetch
$python setup.py install
```

### How to use ###

```
#!python

from quotelib import icici
# Stock symbol for Larsen and Toubro recognized by ICICI as LARTOU
q = icici.get_quote('LARTOU',exchange='NSE') 
print(q.symbol)
print(q.price)
print(q.volume)
print(q.changepct)




```

### Contribution guidelines ###
Open for contributions by fork and suggest patches
Please leave your comments on [my blog](http://www.xerxys.in/p/blog-page_24.html)